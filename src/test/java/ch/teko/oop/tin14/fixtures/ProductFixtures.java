package ch.teko.oop.tin14.fixtures;

import ch.teko.oop.tin14.collection.Product;

/**
 * Created by grazi on 10.05.16.
 */

public class ProductFixtures
{
    public final static Product door = new Product("Wooden Door", 35);
    public final static Product floorPanel = new Product("Floor Panel", 25);
    public final static Product window = new Product("Glass Window", 10);
}
