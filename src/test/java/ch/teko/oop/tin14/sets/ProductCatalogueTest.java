package ch.teko.oop.tin14.sets;

import ch.teko.oop.tin14.collection.Supplier;
import ch.teko.oop.tin14.fixtures.ProductFixtures;
import org.hamcrest.Matchers;
import org.junit.Test;

import static ch.teko.oop.tin14.fixtures.ProductFixtures.door;
import static ch.teko.oop.tin14.fixtures.ProductFixtures.floorPanel;
import static ch.teko.oop.tin14.fixtures.ProductFixtures.window;
import static org.junit.Assert.*;

/**
 * Created by grazi on 16.05.16.
 */
public class ProductCatalogueTest {
    @Test
    public void shouldHoldOnlyUniqueProducts() throws Exception {

        Supplier bobs = new Supplier("Bobs shop");
        bobs.addProduct(door);
        bobs.addProduct(floorPanel);

        Supplier kates = new Supplier("Kates shop");
        kates.addProduct(door);
        kates.addProduct(window);

        ProductCatalogue catalogue = new ProductCatalogue();

        catalogue.isSuppliedBy(bobs);
        catalogue.isSuppliedBy(kates);

        assertThat(catalogue, Matchers.containsInAnyOrder(door, floorPanel, window));
    }

}