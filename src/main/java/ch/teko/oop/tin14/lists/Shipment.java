package ch.teko.oop.tin14.lists;

import ch.teko.oop.tin14.collection.Product;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Shipment implements Iterable<Product> {

    private static final int LIGHT_VAN_MAX_WEIGHT = 20;
    private static final int PRODUCT_NOT_PRESENT = -1;

    private List<Product> products = new ArrayList<>();

    public void add (Product product) {
        products.add(product);
    }

    public void replace (Product oldProduct, Product newProduct) {
        int index = products.indexOf(oldProduct);
        if (index != PRODUCT_NOT_PRESENT) {
            products.set(index, newProduct);
        }


    }

    public void prepare() {
        // TODO implementation
    }

    public List<Product> getHeavyVanProducts() {
        return null;
    }

    public List<Product> getLightVanProducts() {
        return null;
    }

    public Iterator<Product> iterator() {
        return products.iterator();
    }
}
