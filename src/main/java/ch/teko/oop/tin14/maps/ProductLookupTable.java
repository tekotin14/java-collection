package ch.teko.oop.tin14.maps;

import ch.teko.oop.tin14.collection.Product;
import ch.teko.oop.tin14.collection.ProductWithId;

/**
 * Created by grazi on 16.05.16.
 */
public interface ProductLookupTable {

    ProductWithId lookupById (int id);

    void addProduct (ProductWithId productToAdd);

    void clear();
}
