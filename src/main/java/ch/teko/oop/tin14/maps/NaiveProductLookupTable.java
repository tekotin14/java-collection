package ch.teko.oop.tin14.maps;

import ch.teko.oop.tin14.collection.Product;
import ch.teko.oop.tin14.collection.ProductWithId;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grazi on 16.05.16.
 */
public class NaiveProductLookupTable implements ProductLookupTable {

    private List<ProductWithId> products = new ArrayList<>();

    @Override
    public ProductWithId lookupById(int id) {
        for (ProductWithId product : products) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    @Override
    public void addProduct(ProductWithId productToAdd) {
        for (ProductWithId product : products) {
            if (product.getId() == productToAdd.getId()) {
                System.out.println("Sorry, unaible to add Product: " + productToAdd);
                return;
            }
        }
        products.add(productToAdd);
    }

    @Override
    public void clear() {
        products.clear();
    }
}
