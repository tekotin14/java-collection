package ch.teko.oop.tin14.collection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grazi on 16.05.16.
 */
public class Supplier {

    private final String name;
    private List<Product> products = new ArrayList<>();

    public Supplier (String name) {
        this.name = name;
    }

    public void addProduct (Product product) {
        products.add(product);
    }

    public List<Product> products() {
        return products;
    }

}
