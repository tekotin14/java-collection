package ch.teko.oop.tin14.collection;

/**
 * Created by grazi on 16.05.16.
 */
public class ProductWithId extends Product {

    private final int id;

    public ProductWithId(int id, String name, int weight) {
        super(name, weight);
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
