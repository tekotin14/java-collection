package ch.teko.oop.tin14.collection;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Product {

    private final String name;
    private final int weight;

    public Product(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }


    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Product)) {
            return false;
        }
        Product product = (Product) obj;

        return this == obj || name.equals(product.getName()) &&  weight == product.getWeight();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(name)
                .append(weight)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "Product{" +
                "name'" + name + "\'" +
                ", weight=" + weight +
                "}";
    }

}
