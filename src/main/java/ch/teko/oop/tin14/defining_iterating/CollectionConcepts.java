package ch.teko.oop.tin14.defining_iterating;

import ch.teko.oop.tin14.collection.Product;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class CollectionConcepts {

    public static void main(String[] args) {
        Product door = new Product("Wooden Door", 35);
        Product floorPanel = new Product("Floor Panel", 25);
        Product window = new Product("Glass Window", 10);

        List<Product> products = new ArrayList<>();

        products.add(door);
        products.add(floorPanel);
        products.add(window);

        List<Product> subList = products.subList(1, products.size());

        System.out.println(products);

        for (Product product : products) {
            System.out.println(product);
        }

        System.out.println("##### Next Example with Modification of Collectio");

        /*
        for (Product product : products) {
            if (product.getWeight() > 20) {
                System.out.println(product);
            } else {
                products.remove(product);
            }
        }
        */


        /* waste
        for (int i = products.size() - 1 ; i > 0; i--) {
            Product product = products.get(i);
            if (product.getWeight() > 20) {
                System.out.println();
            } else {
                products.remove(i);
            }
        }
        */

        Iterator<Product> productIterator = products.iterator();

        while (products.iterator().hasNext()) {
            Product product = productIterator.next();

            if (product.getWeight() > 20) {
                System.out.println(product);
            } else {
                productIterator.remove();
            }
        }


        System.out.println(products);

        System.out.println(products.contains(window));

        Collection<Product> otherCollection = new ArrayList<>();
        otherCollection.add(window);
        otherCollection.add(door);
        otherCollection.add(new Product("ds", 33));

        products.retainAll(otherCollection);

        System.out.println(products);
    }

}
